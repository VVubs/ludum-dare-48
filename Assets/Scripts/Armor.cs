using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item objects/Armor")]
public class Armor : Item
{

    public enum equip_region
    {
        region_head,
        region_torso,
        region_legs
    }

    [SerializeField] private equip_region _itemEquipRegion;

    public equip_region GetEquipRegion()
    {
        return _itemEquipRegion;
    }

    // Start is called before the first frame update
    void Start()
    {
        _itemType = item_type.type_armor;
    }

}
