using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item : ScriptableObject
{
    // Item type
    public enum item_type
    {
        type_weapon,
        type_armor,
        type_treasure
    }

    // Item attributes
    [SerializeField] protected int _itemID;
    [SerializeField] protected string _itemName;
    [SerializeField] protected Sprite _itemSprite;
    protected item_type _itemType;


    public string GetItemName()
    {
        return _itemName;
    }

    public Sprite GetItemSprite()
    {
        return _itemSprite;
    }

    public item_type GetItemType()
    {
        return _itemType;
    }
    
}
