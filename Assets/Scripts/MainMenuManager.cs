using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{

    public GameObject m_OptionsPanel; // Options panel

    // Starts the game
    public void StartGame()
    {
        SceneManager.LoadScene("Level 1");


    }

    // Show options tab
    public void ShowOptions(bool status)
    {
        m_OptionsPanel.SetActive(status);
    }

    // Quits the game
    public void QuitGame()
    {
        Application.Quit();
    }
}
