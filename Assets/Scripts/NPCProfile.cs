using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCProfile : ScriptableObject
{
    protected enum npc_alignment
    {
        npc_neutral,
        npc_friendly,
        npc_hostile
    }

    [SerializeField] protected int _npcID;
    [SerializeField] protected string _npcName;
    [SerializeField] protected int _npcHealth;
    [SerializeField] protected int _npcAtk;
    [SerializeField] protected int _npcDef;
    [SerializeField] protected Sprite _npcSprite;
    [SerializeField] protected npc_alignment _npcAlignment;

    public int GetNPCHealth()
    {
        return _npcHealth;
    }

    public int GetNPCAttack()
    {
        return _npcAtk;
    }

    public int GetNPCDefense()
    {
        return _npcDef;
    }

    public string GetNPCname()
    {
        return _npcName;
    }

    public Sprite GetNPCSprite()
    {
        return _npcSprite;
    }
}
