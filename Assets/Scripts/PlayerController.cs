using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private GameObject _playerGameObject; // Player game object
    private Camera _cameraComponent; // Main camera
    private Sprite _charSprite; // Player sprite component
    private Vector2 _movementVector; // Movement vector for player
    private Vector2 _lastLookVector; // Direction vector for player
    private float _charSpeed = 5; // Default player speed
    private bool _handlePlayerInput = true;

    // Layer mask for raycasting
    private int _layerMask = 1 << 3;

    // Delegate and event for inventory control
    public delegate void PlayerToggleInventory();
    public static event PlayerToggleInventory OnToggleInv;

    // Delegate and event for picking up items
    public delegate void PlayerPickupItem(WorldItem item);
    public static event PlayerPickupItem OnPickupItem;

    // Start is called before the first frame update
    private void Start()
    {
        _playerGameObject = gameObject;
        _charSprite = GetComponent<Sprite>();
        _cameraComponent = Camera.main;
        _layerMask = ~_layerMask;
    }

    // Update is called once per frame
    private void Update()
    {
        // Reset movement vector every frame
        _movementVector = Vector2.zero;

        if (_handlePlayerInput)
        {
            // Assemble movement vector based on WASD input
            if (Input.GetKey(KeyCode.W))
            {
                _movementVector.y += 1;
            }

            if (Input.GetKey(KeyCode.S))
            {
                _movementVector.y -= 1;
            }

            if (Input.GetKey(KeyCode.D))
            {
                _movementVector.x += 1;
            }

            if (Input.GetKey(KeyCode.A))
            {
                _movementVector.x -= 1;
            }

            if(Input.GetKeyDown(KeyCode.Tab))
            {
                OnToggleInv?.Invoke();
            }

            if(Input.GetKeyDown(KeyCode.E))
            {
                HandleInteract();
            }

            if(_movementVector != Vector2.zero)
            {
                _lastLookVector = _movementVector;
            }
            
        }

        UpdateCameraPosition();
    }

    // Handle movement/physics calculations every frame
    private void FixedUpdate()
    {
        ApplyMovementVector(_movementVector);
    }

    private void ApplyMovementVector(Vector2 movement)
    {
        // Update player position using movement vector and character speed value
        _playerGameObject.transform.position += (Vector3)movement.normalized * Time.deltaTime * _charSpeed;
    }

    private void UpdateCameraPosition()
    {
        // Keep camera positioned over player
        Vector3 temp = new Vector3(_playerGameObject.transform.position.x, _playerGameObject.transform.position.y, _cameraComponent.transform.position.z);

        _cameraComponent.transform.position = temp;
    }

    private void HandleInteract()
    {
        RaycastHit2D result = Physics2D.Raycast(transform.position, _lastLookVector, 5f, _layerMask);
        if (result.collider != null)
        {
            if (result.collider.CompareTag("WorldItem"))
            {
                Debug.Log(result.collider.name);
                OnPickupItem?.Invoke(result.collider.GetComponent<WorldItem>());
                Destroy(result.collider.gameObject);
            }
        }
    }

    public void SetHandleInput(bool input)
    {
        _handlePlayerInput = input;
    }

    public bool GetHandleInput()
    {
        return _handlePlayerInput;
    }
}
