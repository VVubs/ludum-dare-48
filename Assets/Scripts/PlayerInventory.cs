using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    public GameObject textNotificationPrefab;
    public GameObject worldItemPrefab;

    private Canvas _invCanvas; // Canvas to display inventory
    private bool _invIsVisible = false; // Is the inventory currently open?

    
    [SerializeField] private List<PlayerInventorySlot> _invItems;
    [SerializeField] private List<PlayerInventorySlot> _invEquippedItems;
    private int _maxItems = 12;
    private int _currentItems = 0;

    // When the player clicks on an inventory slot
    public void ProcessSlotInteraction(int itemIndex)
    {
        // If slot contains a weapon
        if(_invItems[itemIndex].GetStoredItem().GetItemType() == Item.item_type.type_weapon)
        {
            DropItem(itemIndex);
            // Do weapon stuff
        }

        // If slot contains armor
        else if(_invItems[itemIndex].GetStoredItem().GetItemType() == Item.item_type.type_armor)
        {
            // Create temporary reference
            Armor temp = (Armor)_invItems[itemIndex].GetStoredItem();
            switch (temp.GetEquipRegion())
            {
                case Armor.equip_region.region_head:
                    EquipItem(0, itemIndex);
                    break;

                case Armor.equip_region.region_torso:
                    EquipItem(1, itemIndex);
                    break;

                case Armor.equip_region.region_legs:
                    EquipItem(2, itemIndex);
                    break;
            }
                
        }

        // If slot contains a treasure
        else if(_invItems[itemIndex].GetStoredItem().GetItemType() == Item.item_type.type_treasure)
        {
            DropItem(itemIndex);
        }
    }

    // Equip item
    private void EquipItem(int equipRegion, int itemIndex)
    {
        if(_invEquippedItems[equipRegion].GetStoredItem() == null)
        {
            _invEquippedItems[equipRegion].SetItem(_invItems[itemIndex].GetStoredItem());
        }
    }

    // Show/hide inventory based on bool
    private void ShowInventory()
    {
        if (!_invIsVisible)
            _invCanvas.gameObject.SetActive(true);
        else
            _invCanvas.gameObject.SetActive(false);

        _invIsVisible = !_invIsVisible;

    }

    // Pick up an item in the world and store it in the first open space
    private void PickupItem(WorldItem item)
    {
        // Search for the first empty slot and fill it
        for(int i = 0; i < _maxItems; i++)
        {
            if(_invItems[i].GetStoredItem() == null)
            {
                _invItems[i].SetItem(item.itemResource);
                break;
            }
        }
    }

    // Drop item back into world
    private void DropItem(int itemIndex)
    {
        if (itemIndex < _maxItems) // Out of bounds check
        {
            // Drop item on ground before removing it
            GameObject temp = Instantiate(worldItemPrefab, gameObject.transform.position, Quaternion.identity);
            temp.GetComponent<WorldItem>().itemResource = _invItems[itemIndex].GetStoredItem();
            _invItems[itemIndex].SetBlank();
        }
    }

    private void OnEnable()
    {
        PlayerController.OnToggleInv += ShowInventory;
        PlayerController.OnPickupItem += PickupItem;
    }

    private void OnDisable()
    {
        PlayerController.OnToggleInv -= ShowInventory;
        PlayerController.OnPickupItem -= PickupItem;

    }

    // Start is called before the first frame update
    private void Start()
    {
        //_invItems = new List<PlayerInventorySlot>();
        _invCanvas = GameObject.Find("InventoryCanvas").GetComponent<Canvas>();

        _invCanvas.gameObject.SetActive(false);

    }
}
