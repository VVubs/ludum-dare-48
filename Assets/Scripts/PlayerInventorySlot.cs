using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventorySlot : MonoBehaviour
{

    private Item _storedItem;
    private Image _itemImageComponent;
    private Sprite _itemImageSprite;
    private Sprite _defaultImageSprite; // Blank sprite for empty slots
    private bool _containsItem = false;

    // Set visual representation and holding for item
    public void SetItem(Item itemToSet)
    {
        _storedItem = itemToSet;
        _itemImageComponent.sprite = _storedItem.GetItemSprite();
        _containsItem = true;
    }

    // Set slot back to being empty
    public void SetBlank()
    {
        _storedItem = null;
        _itemImageComponent.sprite = _defaultImageSprite;
        _containsItem = false;
    }

    public Item GetStoredItem()
    {
        if(_storedItem != null)
        {
            return _storedItem;
        }

        else
        {
            return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Setup components
        _itemImageComponent = transform.GetChild(0).GetComponentInChildren<Image>();
        _defaultImageSprite = _itemImageSprite;
    }

    void OnMouseDown()
    {
        if(_containsItem)
        {
            Debug.Log("Dropped item");
        }
    }
}
