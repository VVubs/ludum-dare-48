using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInventory))]
[RequireComponent(typeof(PlayerController))]
public class PlayerManager : MonoBehaviour
{

    // Player statistics
    private int _plrHealth;
    private int _plrAtk;
    private int _plrDef; 
    private int _plrSpeed;

    // Dependent components
    private PlayerInventory _plrInventoryComponent;
    private PlayerController _plrControllerComponent;

    // Start is called before the first frame update
    void Start()
    {
        _plrInventoryComponent = GetComponent<PlayerInventory>();
        _plrControllerComponent = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
