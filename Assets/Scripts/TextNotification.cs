using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextNotification : MonoBehaviour
{
    private TextMeshPro _textComponent;
    
    // Start is called before the first frame update
    void Start()
    {
        _textComponent = GetComponent<TextMeshPro>();
        Destroy(gameObject, 1.5f);
    }

    public void SetText(string text)
    {
        _textComponent.text = text;
    }

}
