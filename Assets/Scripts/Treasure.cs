using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item objects/Treasure")]
public class Treasure : Item
{
    // Start is called before the first frame update
    void Start()
    {
        _itemType = item_type.type_treasure;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
