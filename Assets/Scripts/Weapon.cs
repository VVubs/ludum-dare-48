using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item objects/Weapon")]
public class Weapon : Item
{
    // Start is called before the first frame update
    void Start()
    {
        _itemType = item_type.type_weapon;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
