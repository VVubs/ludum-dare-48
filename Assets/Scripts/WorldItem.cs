using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldItem : MonoBehaviour
{
    // Scriptable object used for configuation
    public Item itemResource;

    // Components
    private SpriteRenderer _itemSpriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        // Setup components
        _itemSpriteRenderer = GetComponent<SpriteRenderer>();
        

        // Destroy if no item resource is supplied
        if(itemResource)
        {
            _itemSpriteRenderer.sprite = itemResource.GetItemSprite();
        }

        else
        {
            Debug.Log("Object was not supplied with any Item-derived ScriptableObject, destroying...");
            Destroy(gameObject);
        }
    }

}
