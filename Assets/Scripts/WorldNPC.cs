using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldNPC : MonoBehaviour
{
    public NPCProfile profileResource;

    private SpriteRenderer _npcSpriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        // Setup components
        _npcSpriteRenderer = GetComponent<SpriteRenderer>();

        // Destroy if no npc profile is supplied
        if(profileResource)
        {
            _npcSpriteRenderer.sprite = profileResource.GetNPCSprite();
        }

        else
        {
            Debug.Log("Object was not supplied with any NPCProfile ScriptableObject, destroying...");
            Destroy(gameObject);
        }
    }

}
